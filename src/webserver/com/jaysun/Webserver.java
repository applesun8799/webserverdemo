package webserver.com.jaysun;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
* @author jaysun
* @version 创建时间:2016年11月3日
*/
public class Webserver {
	
	//模拟web服务器启动，监听客户端请求
	public void serverStart(int port){
		try {
			@SuppressWarnings("resource")
			ServerSocket serverSocket = new ServerSocket(port);
		    while(true){
		    	Socket socket = serverSocket.accept();
		    	//收到client请求后的处理开始
		    	new Processor(socket).start();
		    }
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
    //web服务器启动模拟(监听80端口)
	public static void main(String[] args){
		int port = 80;
//		if(args.length ==1){
//			port = Integer.parseInt(args[0]);
//		}
		new Webserver().serverStart(port);
	}
}
